﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Rigidbody))]
public class Pitch : MonoBehaviour
{
    bool peaked;
    float z_bound = 10;
    float x_bound = 20;
    float z, x;
    Rigidbody rb;

    // Move object using accelerometer
    public float speed = 2.0f;
    private Vector3 dir;
    private float min_y = 3;
    private float max_y = 4;
    public Vector3 vel;
    public GameLogic gameLogic;
    public MeshRenderer renderer;
    Vector3 pos;

    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        rb = GetComponent<Rigidbody>();
        Input.gyro.enabled = true;
    }


    void Update()
    {
        if(!peaked)
        {
            z += Input.gyro.rotationRateUnbiased.z;
            x -= Input.gyro.rotationRateUnbiased.z;

            x = Mathf.Clamp(x, -x_bound, x_bound);
            z = Mathf.Clamp(z, -z_bound, z_bound);


            dir = Vector3.zero;
            dir.y = (Input.acceleration.z + 1);

            if (Mathf.Abs(dir.y) < .1f) //ignore small movements
                dir.y = 0;

            // clamp acceleration vector to unit sphere
            if (dir.sqrMagnitude > 1)
                dir.Normalize();

            dir *= speed;

            // Make it move 10 meters per second instead of 10 meters per frame...
            dir *= Time.deltaTime;

            vel.y -= dir.y;
            if (vel.y < -.1f)
                vel.y = -.1f;
            // Move object
            pos += vel;
            pos.y = Mathf.Clamp(pos.y, min_y, max_y);
        }
        else
        {
            GetComponent<BoxCollider>().enabled = false;
            transform.position = new Vector3(Mathf.Lerp(transform.position.x, -7, Time.deltaTime),transform.position.y,transform.position.z);
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.identity, Time.deltaTime);
            Color color = renderer.material.color;
            color = new Color(color.r, color.g, color.b, Mathf.Lerp(color.a, 0, Time.deltaTime*2));
            renderer.material.color = color;

        }
    }

    void FixedUpdate()
    {
        if(!peaked)
        {
            Quaternion rotation;
            rotation = Quaternion.Euler(-Input.gyro.rotationRateUnbiased.x, 0, Input.gyro.rotationRateUnbiased.z);

            Quaternion newrotation = transform.rotation * rotation;

            rb.MoveRotation(newrotation);
            rb.MovePosition(pos);

            if (transform.position.y == max_y)
            {
                peaked = true;
                gameLogic.CompleteCurrentPitch();
            }
        }
    }


    public void Reset()
    {
        peaked = false;
        GetComponent<BoxCollider>().enabled = true;
        vel = Vector3.zero;
        pos.x = 0;
        pos.y = min_y;
        pos.z = 2.3f;
        transform.position = pos;
        transform.rotation = Quaternion.identity;
        Color color = renderer.material.color;
        color = new Color(color.r, color.g, color.b, 1);
        renderer.material.color = color;
    }
}
