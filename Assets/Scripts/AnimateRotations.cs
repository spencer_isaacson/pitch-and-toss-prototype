﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class AnimateRotations : MonoBehaviour
{
    public float[] timestamps;
    public Quaternion[] poses;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public int i = 0;
    public float time = 0;
    public float transitionLength;
    public float currentTransitionTime;
    public float t;
    // Update is called once per frame
    void Update()
    {
        if(timestamps[i] < time)
        {
            i++;
            if (i == timestamps.Length-1)
            {
                i = 0;
                time = 0;
            }
        }

        transitionLength = timestamps[i + 1] - timestamps[i];
        currentTransitionTime = time - timestamps[i];
        t = currentTransitionTime / transitionLength;
        transform.rotation = Quaternion.Slerp(poses[i], poses[i + 1], t);
        time = + Time.deltaTime;
    }
}
