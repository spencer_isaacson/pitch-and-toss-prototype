﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    private float y_bound = 65;
    private float rot_y;
    GameObject camParent;
    public Transform camera_locator, parent_locator;

    void Awake()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        Input.gyro.enabled = true;
        Input.compass.enabled = true;

        camParent = new GameObject("CamParent");
        camParent.transform.position = transform.position;
        Rigidbody rb = camParent.AddComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.interpolation = RigidbodyInterpolation.Interpolate;
        transform.parent = camParent.transform;

        var initialHeading = Input.compass.magneticHeading; //TODO use heading to hint camera heading
        camera_locator.position = transform.position;
        parent_locator.position = camParent.transform.position;
    }


    void LateUpdate()
    {
        if (SystemInfo.supportsGyroscope)
            rot_y += -Input.gyro.rotationRateUnbiased.y; //replace with actual controls for desktop version
        else
            rot_y += Mathf.Sin(Time.time);
        rot_y = Mathf.Clamp(rot_y, -y_bound, y_bound);
        parent_locator.rotation = Quaternion.identity;
        Vector3 up_vector = -Input.gyro.gravity;
        up_vector.x = -up_vector.x;
        camera_locator.up = up_vector;
        parent_locator.rotation = Quaternion.Euler(0, rot_y, 0);

    }

    void FixedUpdate()
    {

        GetComponent<Rigidbody>().MoveRotation(camera_locator.rotation);
        camParent.GetComponent<Rigidbody>().MoveRotation(parent_locator.rotation);
    }

    //void OnGUI()
    //{
    //    GUI.Label(new Rect(0, 0, 100, 100), "angularVelocity: " + GetComponent<Rigidbody>().angularVelocity);
    //    GUI.Label(new Rect(0, 100, 100, 100), "rot_y: " + rot_y);
    //}
}




