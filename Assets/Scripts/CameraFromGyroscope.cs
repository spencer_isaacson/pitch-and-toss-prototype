﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFromGyroscope : MonoBehaviour
{
    public Transform small;
    public Transform medium;
    public Transform large;

    Transform coin;
    private bool readyToThrow = false;
    private FixedJoint fixedJoint;
    void Start()
    {
        fixedJoint = GetComponent<FixedJoint>();
    }

    public void SetThrow(bool enabled)
    {

        if (readyToThrow && !enabled)
            Toss();

        readyToThrow = enabled;
    }

    void Toss()
    {
        Vector3 angularVelocity = Vector3.one;
        if(coin != null && angularVelocity.magnitude > 1)
        {
            if (coin.GetComponent<Rigidbody>().IsSleeping())
                coin.GetComponent<Rigidbody>().WakeUp();
            GetComponent<FixedJoint>().breakForce = 0;
            GetComponent<FixedJoint>().breakTorque = 0;
            fixedJoint = gameObject.AddComponent<FixedJoint>();
            GetComponent<GameLogic>().AddCoin(coin.gameObject);
            GetComponent<GameLogic>().CompleteCurrentToss();
            coin = null;
        }
    }

    public void SelectCoin(string coinSize)
    {
        if(coin != null)
        {
            Destroy(coin.gameObject);
        }

        switch (coinSize)
        {
            case "Small":
                coin = Instantiate(small);                
                break;
            case "Medium":
                coin = Instantiate(medium);
                break;
            case "Large":
                coin = Instantiate(large);
                break;
        }

        var r = coin?.GetComponent<Rigidbody>();
        coin.GetComponent<Coin>().player = GameLogic.currentPlayer;
        r.isKinematic = false;
        coin.parent = transform;
        coin.localPosition = new Vector3(0, 0, 1.35f);
        coin.localRotation = Quaternion.Euler(-90,0,0);
        coin.parent = null;
        GetComponent<FixedJoint>().connectedBody = r;
    }
}
