﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AwakeVelocity : MonoBehaviour
{
    public Vector3 startVelocity;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody>().velocity = startVelocity;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
