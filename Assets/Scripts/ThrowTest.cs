﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowTest : MonoBehaviour
{
    void Awake()
    {
        if (SystemInfo.supportsGyroscope)
            Input.gyro.enabled = true;

        Input.compass.enabled = true;
    }

    void Update()
    {        
    }

    void OnGUI()
    {
        string message = "";

        if (SystemInfo.supportsGyroscope)
        {
            message = "Gyroscope: " + Input.gyro.attitude.eulerAngles.ToString();
        }
        else
            message = "sorry, no gyro";

        message = "Gyroscope: " + Input.gyro.attitude.eulerAngles.ToString();
        GUI.Label(new Rect(0, 0, 200, 100), message);
    }
}
