﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class RecordRotations : MonoBehaviour
{
    BinaryWriter writer;
    private string filePath;
    public bool record = false;
    // Start is called before the first frame update
    void Start()
    {
        filePath = Application.persistentDataPath + "/coinrotations.txt";
        if (record)
            writer = new BinaryWriter(new FileStream(filePath, FileMode.Create));
    }

    float t = 0;
    float i = 0;
    float duration = 10;
    float interval = 1 / 60.0f;
    // Update is called once per frame
    void Update()
    {
        if (record)
        {
            t += Time.deltaTime;
            i += Time.deltaTime;
            if (i > interval)
            {
                i = 0;
                if (t < duration)
                {
                    writer.Write(transform.rotation.x);
                    writer.Write(transform.rotation.y);
                    writer.Write(transform.rotation.z);
                    writer.Write(transform.rotation.w);
                }
                else
                {
                    writer.Close();
                }
            }
        }
    }

    void OnGUI()
    {
        GUI.Label(new Rect(0, 400, 1000, 400), filePath);
    }
}
