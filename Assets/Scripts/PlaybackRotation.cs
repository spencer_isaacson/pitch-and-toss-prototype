﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class PlaybackRotation : MonoBehaviour
{
    BinaryReader reader;
    private string filePath;
    // Start is called before the first frame update
    void Start()
    {
        filePath = Application.persistentDataPath + "/coinrotations.txt";
        reader = new BinaryReader(new FileStream(filePath, FileMode.Open));
    }

    Quaternion current = Quaternion.identity;
    float t = 0;
    float i = 0;
    float duration = 10;
    float interval = 1 / 60.0f;
    // Update is called once per frame
    void Update()
    {
        i += Time.deltaTime;

        if (i > interval)
        {
            i = 0;
            if (reader.BaseStream.Position != reader.BaseStream.Length)
            {
                current.x = reader.ReadSingle();
                current.y = reader.ReadSingle();
                current.z = reader.ReadSingle();
                current.w = reader.ReadSingle();
                current.Normalize();
            }
            else
            {
                reader.Close();
                enabled = (false);
            }
        }
    }

    void FixedUpdate()
    {
        GetComponent<Rigidbody>().MoveRotation(current);
    }

    void OnGUI()
    {
        GUI.Label(new Rect(0, 100, 100, 100), GetComponent<Rigidbody>().angularVelocity.ToString());
    }
}