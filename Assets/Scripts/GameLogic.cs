﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour
{
    enum Phase
    {
        Pitch, Toss,
    }

    Phase currentPhase;

    public static int currentPlayer = 1;
    public const int playerCount = 4;
    public GameObject pitchGuards;
    public GameObject tossGUI;
    public GameObject pitchHand;
    public List<GameObject> coins = new List<GameObject>();
    public GameObject LineProjector;
    public Transform distanceIndicatorsCanvas;
    public Transform textPrefab;
    private List<GameObject> distanceIndicators = new List<GameObject>();
    public int[] scores = new int[playerCount];
    // Start is called before the first frame update
    void Start()
    {
        currentPhase = Phase.Toss;
    }

    // Update is called once per frame
    void Update()
    {
        if (waitingToCompleteToss)
        {
            if (coins[coins.Count - 1].GetComponent<Rigidbody>().IsSleeping())
            {
                NextToss();
            }
        }
        if(waitingToCompletePitch)
        {
            bool allCoinsSleeping = true;
            foreach (GameObject coin in coins)
            {
                if (!coin.GetComponent<Rigidbody>().IsSleeping())
                {
                    allCoinsSleeping = false;
                    break;
                }
            }

            if (allCoinsSleeping)
            {
                for (int i = 0; i < coins.Count; i++)
                {
                    if (coins[i].transform.up.y > 0)
                        scores[currentPlayer-1]++;
                }

                for (int i = 0; i < coins.Count; i++)
                {
                    mylabel:
                    if (coins[i].transform.up.y > 0)
                    {
                        GameObject coin = coins[i];
                        coins[i] = coins[coins.Count - 1];
                        coins.RemoveAt(coins.Count - 1);
                        distanceIndicators.RemoveAt(distanceIndicators.Count - 1);
                        Destroy(coin);
                        if(i < coins.Count)
                            goto mylabel;
                    }
                }

                NextPitch();
            }
        }

        for(int i = 0; i < coins.Count;i++)
        {
            if(distanceIndicators[i].activeSelf)
            {
                distanceIndicators[i].GetComponent<Text>().transform.position = Camera.main.WorldToScreenPoint(coins[i].transform.position);
                distanceIndicators[i].GetComponent<Text>().text = (5- coins[i].transform.position.z).ToString("F2");
            }
        }
    }

    internal void AddCoin(GameObject gameObject)
    {
        coins.Add(gameObject);
        var foo = Instantiate(textPrefab);
        foo.parent = distanceIndicatorsCanvas;
        distanceIndicators.Add(foo.gameObject);
        
    }

    void SetupToss()
    {
        //put camera under player control
        GetComponent<CameraControl>().enabled = true;

        //disable guards
        pitchGuards.SetActive(false);
        tossGUI.SetActive(true);

    }

    public int[] rankorder = new int[playerCount];
    public int rankedplayer;
    void SetupPitch()
    {
        currentPhase = Phase.Pitch;
        //position camera
        GetComponent<CameraControl>().enabled = false;
        transform.rotation = Quaternion.identity;

        //set up guards to prevent pitching out of arena
        pitchGuards.SetActive(true);

        tossGUI.SetActive(false);
        pitchHand.SetActive(true);
        LineProjector.SetActive(false);
        distanceIndicatorsCanvas.gameObject.SetActive(false);
        //rank order players
        coins.Sort((x, y) =>
        {
            if (x.transform.position.z - y.transform.position.z < 0)
                return 1;
            else
                return -1;
        });

        for (int i = 0; i < playerCount; i++)
        {
            rankorder[i] = coins[i].GetComponent<Coin>().player;
        }

        rankedplayer = -1;
        NextPitch();


    }

    bool waitingToCompleteToss = false;
    bool waitingToCompletePitch = false;


    public void CompleteCurrentToss()
    {
        waitingToCompleteToss = true;
        tossGUI.SetActive(false);
    }

    public void CompleteCurrentPitch()
    {
        waitingToCompletePitch = true;
    }

    void NextPitch()
    {
        if(coins.Count > 0)
        {
            rankedplayer++;
            if (rankedplayer == rankorder.Length)
                rankedplayer = 0;

            pitchHand.GetComponent<Pitch>().Reset();
            waitingToCompletePitch = false;
            currentPlayer = rankorder[rankedplayer];
            foreach (GameObject coin in coins)
            {
                //todo get rid of magic numbers
                float x = coin.transform.position.x / 10;
                x = Mathf.Clamp(x, -.5f, .5f);
                float z = coin.transform.position.z / 10;
                z = Mathf.Clamp(z, -.5f, .5f);
                z += 2.3f;
                coin.transform.position = new Vector3(x, 4.4f, z);
            
            }
        }
    }

    void NextToss()
    {
        waitingToCompleteToss = false;
        SetClosestCoin();
        currentPlayer++;
        tossGUI.SetActive(true);
        if (currentPlayer > playerCount)
        {
            currentPlayer = 1;
            SetupPitch();
        }
    }

    void SetClosestCoin()
    {
        LineProjector.SetActive(true);
        float z = -1000;

        foreach (GameObject coin in coins)
        {
            if (coin.transform.position.z > z)
                z = coin.transform.position.z;
        }
        LineProjector.transform.position = new Vector3(LineProjector.transform.position.x, LineProjector.transform.position.y, z);
    }

    void OnGUI()
    {
        GUI.Label(new Rect(0, 30, 200, 30), $"Player {currentPlayer}, {currentPhase}!");
        for (int i = 0; i < playerCount; i++)
        {
            GUI.Label(new Rect(0, 60+i*12, 200, 30), $"player {i+1}: {scores[i]}");

        }

        if (currentPhase == Phase.Pitch && coins.Count == 0)
            GUI.Label(new Rect(0, 0, Screen.width, Screen.height), "All Coins Collected", new GUIStyle() { alignment = TextAnchor.MiddleCenter });
    }
}
