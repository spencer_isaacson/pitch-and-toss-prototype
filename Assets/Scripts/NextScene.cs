﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextScene : MonoBehaviour
{
    int currentscene = 1;
    public int sceneCount;
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoToNextScene()
    {
        SceneManager.LoadScene(currentscene++);
        
        if (currentscene > sceneCount)
            currentscene = 1;
    }
}
