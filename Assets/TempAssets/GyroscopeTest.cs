﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroscopeTest : MonoBehaviour
{
    //TODO turn into proper "orientation gizmo" script
    public LineRenderer right, up, forward;
    public bool foo = true;
    private Quaternion north;
    // Start is called before the first frame update
    void Start()
    {
        Input.compass.enabled = true;
        Input.gyro.enabled = true;

        north = Quaternion.Euler(0, Input.compass.magneticHeading, 0);
    }

    // Update is called once per frame
    void Update()
    {
        Quaternion north_new = Quaternion.Euler(0, Input.compass.magneticHeading, 0);
        north = Quaternion.Slerp(north, north_new, Time.deltaTime);

        Vector3 up_vector = Vector3.up;
        if(SystemInfo.supportsGyroscope && Input.gyro.enabled)
            up_vector = -Input.gyro.gravity;
        Vector3 forward_vector = north * Vector3.forward;
        Vector3 right_vector = Vector3.Cross(up_vector, forward_vector);

        right.SetPositions(new Vector3[] { Vector3.zero, right_vector });
        up.SetPositions(new Vector3[] { Vector3.zero, up_vector });
        forward.SetPositions(new Vector3[] { Vector3.zero, forward_vector });
    }


    void FixedUpdate()
    {
        if(foo)
        {
            Quaternion rotation;
            rotation = Quaternion.Euler(-Input.gyro.rotationRateUnbiased.x, 0, Input.gyro.rotationRateUnbiased.z);
        
            Quaternion newrotation = transform.rotation * rotation;

            GetComponent<Rigidbody>().MoveRotation(newrotation);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawLine(Vector3.zero, Input.gyro.gravity);
    }

    void OnGUI()
    {
        GUI.Label(new Rect(0, 120, 200, 100), "gravity: " + Input.gyro.gravity);
        GUI.Label(new Rect(0, 140, 200, 100), "north: " + Input.compass.trueHeading);
    }
}
