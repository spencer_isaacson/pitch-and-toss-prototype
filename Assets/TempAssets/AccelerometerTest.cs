﻿using UnityEngine;

public class AccelerometerTest : MonoBehaviour
{
    // Move object using accelerometer
    public float speed = 10.0f;
    private Vector3 dir;
    private float min_y = 0;
    private float max_y = 5;
    private Vector3 vel;
    void Update()
    {
        dir = Vector3.zero;
        dir.y = (Input.acceleration.z+1);

        if (Mathf.Abs(dir.y) < .1f) //ignore small movements
            dir.y = 0;

        // clamp acceleration vector to unit sphere
        if (dir.sqrMagnitude > 1)
            dir.Normalize();

        dir *= speed;
        
        // Make it move 10 meters per second instead of 10 meters per frame...
        dir *= Time.deltaTime;

        vel.y -= dir.y;
        if (vel.y < -.1f)
            vel.y = -.1f;

        vel.y -= Time.deltaTime;
        // Move object
        transform.Translate(vel);
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, min_y, max_y), transform.position.z);
    }

    void OnGUI()
    {
        GUI.Label(new Rect(0, 0, 600, 100), "acceleration: " + Input.acceleration.ToString("F5"));
        GUI.Label(new Rect(0, 32, 600, 100), "velocity: " + vel.ToString("F5"));
    }
}